﻿#
# WebPerformer Configuration File
#
conf.encoding=UTF-8

# システム設定値のインクルード
include=wptool_base.conf

# Antに与える最大メモリ
tool.ant.maxmem=512m

# ウェブサービスのマッピングテンプレートファイルを
# 生成する際に使用するAntに与える最大メモリ
tool.ant.ws.maxmem=256m

# ビルド単位
tool.ant.build.unit=100

# JSP分割
tool.jsp.divide.unit=10

# アクション実行時初期値の再計算を行うか（Default:true）
tool.request.recalc=true

# 画面上部メッセージ一行表示
# false : 全行表示する（Default）
# true  : 最初の一行を表示し、クリックすると全行表示する
#tool.message.singleline=false

# データ未検索時のメッセージ表示／非表示フラグ
# 0:未検索時に表示しない（Default）
# 1:未検索時も表示する
# 2:未検索時、検索で0件時も表示しない
initGroupMessageDisplay=0

# サーバと送受信中に「送信中です」「受信中です」を表示する (true:表示する, false:表示しない　デフォルト:true)
#tool.sendrecvmessage.display=false

# ブラウザの「戻る」、「更新」をした場合にエラーとする(true:エラーとする, false:エラーとしない　デフォルト:false)
#tool.check.browser=true

# テーブル名にスキーマ名を付加するかどうか(true:付加する, false:付加しない　デフォルト:true)
# データベースにアクセスする際、およびDDLを生成する際に有効です。
#####For iSeriesPack #####
target.table.schema=false

#内部演算の小数の精度
tool.operation.scale=10

# 生成アプリケーションのデータ文字エンコーディング(Windows-31J, UTF-8)
target.encoding=UTF-8

# 生成アプリケーションが使用するMessageResourcesファイル名
tool.MessageResources=MessageResources.conf

# 生成アプリケーションの入力モードを切り替えます。
# 通貨型フォーマット記号、日付フォーマットを変更します。
# |値|記号|日付形式(YMD/MDY)|         |         |
# |0 | \  |      YMD       |デフォルト|日本向け  |
# |1 | $  |      YMD       |         |米国向け  |
# |2 | $  |      MDY       |         |米国向け２|
# |3 | \  |      YMD       |         |中国向け  |
#tool.input.locale=0

# 通貨型フォーマット記号を指定します。
# 指定がない場合は tool.input.locale で指定された通貨型フォーマット記号を使用します。
#tool.locale.currency.sign=

# 言語毎の日付フォーマット記号を「|」区切りで指定します。 (言語:YMD, 言語:MDY)
# 言語には「ApplicationResources_言語.conf」 の言語部分の文字列を指定してください。
# 指定がない場合は tool.input.locale で指定された日付フォーマットを使用します。
#tool.locale.dateformat=ja:YMD|en:MDY|zh:YMD

# 行追加／行削除ボタンにカーソル移動を行うかどうかを指定します。
# NONE：移動しません。行追加／行削除実行後のカーソル位置はその直前の項目になります。（V112までの動作）
# ADDDEL_SEL：行追加／行削除ボタンに移動します。行追加／行削除実行後のカーソル位置はそれぞれのボタンになります。(デフォルト)
tool.cursor.group=ADDDEL_SEL

# エクスポート入出力においてファイル名とする項目の入出力項目コードを指定します。
# 詳細は定義ガイドをご覧ください。
tool.export.filenamecode=EXPORT_FILE_NAME

# エクスポート入出力においてシート名とする項目の入出力項目コードを指定します。
# 詳細は定義ガイドをご覧ください。
tool.export.sheetnamecode=EXPORT_SHEET_NAME

# CODE型に半角スペースを許可するか。
# 0：許可しない。（Default）
# 1：許可する。
#tool.dtcode.space=1

# ステータスバーの表示モード(STATUSBAR_MODE)を設定します。
# STATUSBAR表示モード
# 0:非表示
# 1:全表示（Default）
# 2:エラーメッセージのみ表示
#tool.statusbar.mode=1

# labelStatement サニタイジング
# false : サニタイジングしない。（Default）
# true  : サニタイジングする。
#tool.sanitize.labelstatement=false

# モバイル画面において、U+005C（バックスラッシュ）の文字コードに対して、円マークとバックスラッシュのどちらを表示するか
# true : U+005Cを文字実体参照&yen;に変換する。ブラウザーは円マークを表示する。（Default）
# false: U+005Cをそのまま送る。ブラウザーはバックスラッシュを表示する。
tool.output.mobile.005C_to_yen=true

# PC画面において、円記号にU+00A5（円マーク）とU+005C（バックスラッシュ）のどちらを使うか
# true : PC画面の円記号にU+005Cを使用する。U+00A5をU+005C（バックスラッシュ）に変換する。（Default）
# false: PC画面の円記号にU+00A5を使う。文字コード変換を行わない。
tool.output.pc.00A5_to_005C=true

# Java ファイルの文字エンコーディングを指定します。(Windows-31J, UTF-8)
# 指定がない場合、target.encoding と同じエンコーディングを使用します。
tool.java.file.encoding=UTF-8

# カレンダー表示時の現地時間補正を行うかどうかを指定します。
# 0:補正せずサーバー時間を使用する。（Default）
# 1:現地時間で補正する。
#tool.locale.datetime.calendar=0

# SQL Server のTEXT型項目のデータ型を指定します。(VARCHAR, NVARCHAR)
# 指定がない場合、VARCHARを使用します。
#sqlserver.text.column.type=NVARCHAR

#表示フィールド係数
tool.sizefactor.code=1.3872
tool.sizefactor.text=1.3872
tool.sizefactor.num=1.15
tool.sizefactor.currency=1.15
tool.sizefactor.date=1.15
tool.sizefactor.time=1.1
tool.sizefactor.file=1.3

# 次入出力条件分岐アクションのエラー表示有無を指定します。
# 特に、以下のケースでの動作を選択します。
# ・定義に、メッセージコードNGの設定がない
# ・実行時に、合致する次入出力条件があった
# true：「システムエラー画面」に遷移する
# false：エラーは表示せず、「合致条件の次入出力画面」に遷移する（デフォルト）
#tool.nextiologic.errorpage=true

# Webサービス入出力でRESTを無効化するときtrueを指定します。
tool.wsio.rest.disabled=false
